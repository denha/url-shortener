var express = require("express");
var path = require("path");
var mongodb = require("mongodb");
var validUrl = require("valid-url");
var shortid = require('shortid');

var app = express();
app.use(express.static(__dirname + "/public"));

// Create a database variable outside of the database connection callback to reuse the connection pool in your app.
var db;

// Connect to the database before starting the application server.
mongodb.MongoClient.connect(process.env.MONGOLAB_URI || "mongodb://localhost:27017", function (err, database) {
    if (err) {
        console.log(err);
        process.exit(1);
    }

    // Save database object from the callback for reuse.
    db = database;
    console.log("Database connection ready");

    // Initialize the app.
    var server = app.listen(process.env.PORT || 8080, function () {
        var port = server.address().port;
        console.log("App now running on port", port);
    });
});

// Generic error handler used by all endpoints.
function handleError(res, reason, message, code) {
    console.log("ERROR: " + reason);
    res.status(code || 500).json({"error": message});
}

app.get("/", function (req, res) {
    res.sendFile(path.join(__dirname, '/index.html'));
    // res.sendFile(__dirname + '/index.html');
});

app.get("/*/", function (req, res) {
    var path = req.url.substring(1);
    if (validUrl.isUri(path)) {
        var newURL = {
            "_id": shortid.generate(),
            "redirect": path
        };
        db.collection("urls").insertOne(newURL, function (err, doc) {
            if (err) {
                handleError(res, err.message, "Failed to create new url.");
            } else {
                res.status(201).json({
                    original_url: path,
                    short_url: req.headers.hostname || req.headers.host + '/' + newURL._id
                });
            }
        })
    } else {
        db.collection("urls").find({_id: path}).limit(1).next(function(err, doc) {
            if (err) {
                handleError(res, err.message, "Failed to get contact");
            } else if(doc) {
                res.redirect(doc.redirect);
            } else {
                res.send('<code>' + path + '</code><p>is not a valid URL or a shortened URL.</p>');
            }
        });
    }
});